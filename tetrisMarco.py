# -*- coding:utf-8 -*-
alto = int(25)
ancho = int(10)
valor = '50'

# Recuadro tetris
spaces = ancho-2

for elem in range(alto):
	if( elem == 1 ):
		print ( '%s%s%s%s%s' % ( ('*  '), ('  '*(spaces-3)), ('Puntaje: '+valor), (' '*3), ('*  ') ) )
		continue
	if( elem == 0 or elem == alto-1):
		print ( '%s' % ('*  '*ancho) )
	else:
		print ( '%s%s%s' % ( ('*  '), ('   '*spaces), ('*  ') ) )
