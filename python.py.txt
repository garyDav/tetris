# Escribir >python en la terminal para ver si ya tienes instalado python
# >>> si te sale esto estas en el pronpt de python
#Si te sale un error procedemos a instalar
#pagina www.python.org/download
#Si quieres utilizar IDE
#http://pydev.org/
#otra alternativa instalar aptana studio 3

print 'Hola Ppython'

#Ejecutar archivos te python
#Ubicarse en la ruta donde esta tu archivo.py
#Escribir >python archivo.py

########### ENTEROS, REALES y OPERADORES ARITMETICOS ###########
int 	#+-2.147.483.648 plataformas de 32bits +-9x10-18 plataformas de 64bits
long 	#Limitado por la memoria disponible

#Como darle valor int a una variable en python
a = 5
#Como darle valor long a una variable en python
b = 5L
#Numero float
real = 0.567
#Multiplica el numero flotante por diez a la -3
real = 0.56e-3
#Pranticando con operadores
a = 26
b = 11.3
c = 5
d = 3.5
#SUMA
print a+b
#Resta
print c-a
#Multiplicación
print d*c
#Exponente
print c ** 2
#División uno de las variables tiene que ser float
print float(c) / a
#División entera si ambas variables son enteras
print 7 / 3
#Modulo
print 7 % 3

########### BOOLEANOS, OPERADORES LOGICOS y CADENAS ###########
#Comillas simples
cads = 'Texto entre comillas simples'
print cads
#Comillas dobles
cadd = "Texto entre comillas dobles"
print cadd
#Si queremos ver cual es el tipo de la variable
print type(cads)

#Que pasa si queremos poner interlineado
cads = 'Texto en una linea. 
		Texto en otra linea.'

#Nos genera un error
#Podriamos cerrar con comilla o bien colocar en dos variables las cadenas
#Pero eso no queremos...
#La mejor alternativa es usar caracteres de escape
cads = 'Texto en una linea.\nTexto en otra linea.'
print cads
#Barra invertida n es para interlinear texto
#Otra barra de escape \t es para tabulación
cads = 'Texto en una linea.\n\t Texto en otra linea.'
print cads
#tambien podriamos encerrar la cadena entre comillas simples
cadc = """Texto linea 1
Linea 2
	Linea 3 con tabulación
. 
.
.
Linea N
"""
print cadc

## Repeticion y concatenación ##
#Repetirá 'Cadena' tres veces 
cad  = "Cadena " * 3
print cad
#Concatenación esto solo es posible si todas las variables son de tipo cadena
cad1 = 'Hola'
cad2 = ' como está.'
print cad1+cad2
cad3 = cad1+cad2
print cad3
#Esto me generaría un error
print 'Cadena número: '+4
#Lo correcto sería
print 'Cadena número: '+str(4)
## Variables tipo booleanos
boolT = True
boolF = False
## Operadores Lógicos ##
bAnd = boolT and boolF
print bAnd

bOr = boolT or boolF
print bOr

bNot = not True
print bNot

########### LISTAS, TUPLAS y DICCIONARIOS ###########
#Las listas son como los vectores (array) en otros lenguajes
#Parecido a javascript, las listas pueden tomar diferentes tipos
#de varables en cada elemento.
lista  = [2,'tres',True,["uno",10L],0.79]
print lista

#Para acceder a un elemento de la lista se hace a traves de su índice
#el índice comienza siempre desde cero -> '0'
elem = lista[1]
print elem

#Para acceder a un elemento que está en otra lista
elem = lista[3][0]
print elem

#Cambiar el contenido de una lista
lista[2] = False
print lista

#No importa que el tipo de variable a cambiar sea el mismo que el anterior
lista[1] = 3.55
lista[0] = 'dos'
print lista

#Algo muy interesante con python es que podemos tomar un rango de elementos
#y agregarlo a otra lista de una manera muy sencilla
#lista[deDonde:cuantosElementos] 
sublista = lista[0:3]
print sublista

#También podemos tomar un elemento si y otro no
#lista[deDonde:cuantos:longitudDeSalto+1]
#Si queremos obtener elemenos con un salto de numero, es decir
#uno si y otro no
longSalto = 1
sublista = lista[0:3:longSalto+1]
#También
sublista = lista[0:3:2]
print sublista
#Si queremos que vaya saltando de dos en dos será: lista[0:3:3]
#y así sucesivamente...

#Si queremos copiar todos los elementos a partir del indice -> '0'
sublista = lista[0::2]
print sublista

#Con esta misma estructura podemos cambiar elementos de una lista
#Si quisieramos cambiar los dos primeros elementos de la lista por otra
#necesariamente tiene que ser lista
lista  = [2,'tres',True,["uno",10L],0.79] #teniendo esta lista
lista[0:2] = [] #importante lista[0:2] = [son estos corchetes] 
#No podemos reemplazar unos elementos de una lista por una variable directa
#Este ejemplo seria un error
lista[0:2] = 3
#Este ejemplo no genera error pero toma cada caracter como un elemento
#dentro de la lista, así que hay que tener cuidado...
lista[0:2] = 'Solo una cadena'
#Lo Correcto sería
lista[0:2] = ['Solo una cadena']
#También podríamos reemplazar un rango de elementos por un rango mayor y mas complejo
lista[0:2] = ['Elemento uno',2,['Elementos multiples',False]]

#Otra forma es acceder a los elementos de forma inversa
#a traves de números negativos
lista  = [2,'tres',True,["uno",10L],0.79] #teniendo esta lista
sublista = lista[-1] #el menos uno representa el último elemento de la lista
print sublista
#-1 el último, -2 el penúltimo, -3 el antepenúltimo,..., -n

## Tupla ##
#La forma como se crea una tupla es.
tupla = 1,'Tres',2.5,True,56L
#Pero la mejor manera de crear una tupla es con paréntesis
tupla = (1,'Tres',2.5,True,56L)
print type(tupla)
#La utilización de una tupla es similar a la de la lista.
#La diferencia es que en la tupla no se puede agregar o actualizar un elemento como en las listas
#Este ejemplo me daría un error
tupla[0] = 2
#Error...

## Diccionarios ##
#La sintaxis es igual que un JSON
#La forma de crear un diccionario es.
diccionario = {
	'key': 'value',
	'clave': ['valor multiple',True,54]
}
print diccionario
#Es necesario colocar comillas simples o dobles en la clave
#La Manera de acceder a un valor es a traves de su clave, no existen índices en los diccionarios
diccionario['key'] = 'otro valor';
print diccionario
#para acceder al valor 54
print diccionario['clave'][2]
#A diferencia de las tuplas y de las listas no es posible hacer slice
#que es: diccionario[0:2] no se puede hacer eso por que en los diccionarios no existe el índice
#diccionario[indice:indice]

########### Sentencias condicionales, bubles y funciones ###########
#Sintaxis
if '47' == 47:
	print 'es igual'
else:
	print 'no es igual'

#Si hay varias condiciones dentro de una, acotar la forma if else if a solo if elif
if condicion:
	#hacer algo 
	print 'algo'
elif otracondicion:
	print 'otro'
elif otracondicionN:
	print 'cuandición'

